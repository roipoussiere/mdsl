from mdsl.core import Mdsl, MdslArgumentError
from mdsl.features._fail import mdsl_fail


def mdsl_assert(context: Mdsl, args):
    condition, msg = context.arg_parser(args, [
        ('is', None),
        ('msg', '')
    ])

    if not condition:
        msg = 'AssertionError' + (f': { msg }' if msg else '')
        mdsl_fail(context, { 'msg': msg })


class TestPrint:
    from mdsl.mdsl_test import MdslTest
    test = MdslTest({ 'assert': mdsl_assert })

    def test_assert_no_array(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- assert:")

    def test_assert_empty(self):
        import pytest
        with pytest.raises(MdslArgumentError):
            self.test.run("- assert: []")

    def test_assert_false(self, capsys):
        import pytest
        with pytest.raises(SystemExit):
            self.test.run("- assert: [ false ]")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == 'AssertionError\n'

    def test_assert_true(self, capsys):
        self.test.run("- assert: [ true ]")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == ''

    def test_assert_false_message(self, capsys):
        import pytest
        with pytest.raises(SystemExit):
            self.test.run("- assert: { is: false, msg: 'oops' }")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == 'AssertionError: oops\n'

    def test_assert_true_message(self, capsys):
        self.test.run("- assert: { is: true, msg: 'oops' }")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == ''

    def test_assert_array_form(self, capsys):
        import pytest
        with pytest.raises(SystemExit):
            self.test.run("- assert: [ false, 'oops' ]")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == 'AssertionError: oops\n'
