from mdsl.core import Mdsl


def mdsl_print(context: Mdsl, args):
    '''
    Print data to the standard output.
    Args:
    - msg: the data to print to stdout. Can be an array or a primitive type.
    - sep: the separator, if `msg` is an array (default: ' ').
    - end: the string suffix (default: '\n')
    Returns: nothing
    '''

    msg, sep, end = context.arg_parser(args, [
        ('msg', ''),
        ('sep', ' '),
        ('end', '\n')
    ])
    msg = [ '' if msg is None else msg ] if type(msg) != list else msg
    print(*msg, sep=sep, end=end)


class TestPrint:
    from mdsl.mdsl_test import MdslTest
    test = MdslTest({ 'print': mdsl_print })

    def test_print_no_array(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- print: 'hello'")

    def test_print_empty(self, capsys):
        self.test.run("- print: []")
        captured = capsys.readouterr()

        assert captured.out == '\n'
        assert captured.err == ''

    def test_print_message(self, capsys):
        self.test.run("- print: [ 'hello' ]")
        captured = capsys.readouterr()

        assert captured.out == 'hello\n'
        assert captured.err == ''

    def test_print_end(self, capsys):
        self.test.run("- print: { msg: 'hello', end: '' }")
        captured = capsys.readouterr()

        assert captured.out == 'hello'
        assert captured.err == ''

    def test_print_sep(self, capsys):
        self.test.run("- print: { msg: [ 'hello', 'world' ], sep: '... ' }")
        captured = capsys.readouterr()

        assert captured.out == 'hello... world\n'
        assert captured.err == ''

    def test_print_array_args(self, capsys):
        self.test.run("- print: [ [ 'hello', 'world' ], '... ', '' ]")
        captured = capsys.readouterr()

        assert captured.out == 'hello... world'
        assert captured.err == ''
