from mdsl.core import Mdsl
from mdsl.core import MdslArgumentError


def mdsl_lt(context: Mdsl, data):
    '''
    Lower than comparator
    Args: variable number of items.
    Returns: The array or string
    '''

    a, b = context.arg_parser(data, [
        ('a', None),
        ('b', None)
    ])

    return a < b

def mdsl_gt(context: Mdsl, data):
    '''
    Greater than comparator
    Args: variable number of items.
    Returns: The array or string
    '''

    a, b = context.arg_parser(data, [
        ('a', None),
        ('b', None)
    ])

    return a > b

def mdsl_le(context: Mdsl, data):
    '''
    Lower or equal comparator
    Args: variable number of items.
    Returns: The array or string
    '''

    a, b = context.arg_parser(data, [
        ('a', None),
        ('b', None)
    ])

    return a <= b

def mdsl_ge(context: Mdsl, data):
    '''
    Greater or equal than comparator
    Args: variable number of items.
    Returns: The array or string
    '''

    a, b = context.arg_parser(data, [
        ('a', None),
        ('b', None)
    ])

    return a >= b

def mdsl_eq(context: Mdsl, data):
    '''
    Equal comparator
    Args: variable number of items.
    Returns: The array or string
    '''

    a, b = context.arg_parser(data, [
        ('a', None),
        ('b', None)
    ])

    return a == b

def mdsl_ne(context: Mdsl, data):
    '''
    Not equal comparator
    Args: variable number of items.
    Returns: The array or string
    '''

    a, b = context.arg_parser(data, [
        ('a', None),
        ('b', None)
    ])

    return a != b

class TestPrint:
    from mdsl.mdsl_test import MdslTest
    test = MdslTest({
        'lt': mdsl_lt,
        'gt': mdsl_gt,
        'le': mdsl_le,
        'ge': mdsl_ge,
        'eq': mdsl_eq,
        'ne': mdsl_ne
    })

    def test_lt(self):
        assert     self.test.run("- lt: [ 1, 2 ]")
        assert not self.test.run("- lt: [ 2, 2 ]")
        assert not self.test.run("- lt: [ 3, 2 ]")

    def test_gt(self):
        assert not self.test.run("- gt: [ 1, 2 ]")
        assert not self.test.run("- gt: [ 2, 2 ]")
        assert     self.test.run("- gt: [ 3, 2 ]")

    def test_le(self):
        assert     self.test.run("- le: [ 1, 2 ]")
        assert     self.test.run("- le: [ 2, 2 ]")
        assert not self.test.run("- le: [ 3, 2 ]")

    def test_ge(self):
        assert not self.test.run("- ge: [ 1, 2 ]")
        assert     self.test.run("- ge: [ 2, 2 ]")
        assert     self.test.run("- ge: [ 3, 2 ]")

    def test_eq(self):
        assert not self.test.run("- eq: [ 1, 2 ]")
        assert     self.test.run("- eq: [ 2, 2 ]")
        assert not self.test.run("- eq: [ 3, 2 ]")

    def test_ne(self):
        assert     self.test.run("- ne: [ 1, 2 ]")
        assert not self.test.run("- ne: [ 2, 2 ]")
        assert     self.test.run("- ne: [ 3, 2 ]")
