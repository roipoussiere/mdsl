from mdsl.core import Mdsl
from mdsl.core import MdslArgumentError

def mdsl_add(context: Mdsl, args):
    if len(args) < 2:
        raise MdslArgumentError('At least 2 parameters are required')

    return sum(context.arg_parser(args, []))

def mdsl_sub(context: Mdsl, args):
    a, b = context.arg_parser(args, [ ('a', None), ('b', None) ] )
    return a - b

def mdsl_mul(context: Mdsl, args):
    if len(args) < 2:
        raise MdslArgumentError('At least 2 parameters are required')

    res = 1
    for arg in context.arg_parser(args, []):
        res = res * arg
    return res

def mdsl_div(context: Mdsl, args):
    a, b = context.arg_parser(args, [ ('a', None), ('b', None) ] )
    return a / b

def mdsl_mod(context: Mdsl, args):
    a, b = context.arg_parser(args, [ ('a', None), ('b', None) ] )
    return a % b

def mdsl_exp(context: Mdsl, args):
    a, b = context.arg_parser(args, [ ('a', None), ('b', None) ] )
    return a ** b


class TestSimpleMaths:
    from mdsl.mdsl_test import MdslTest
    test = MdslTest({
        'add': mdsl_add,
        'sub': mdsl_sub,
        'mul': mdsl_mul,
        'div': mdsl_div,
        'mod': mdsl_mod,
        'exp': mdsl_exp
    })

    def test_add(self):
        import pytest
        

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- add: []")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- add: [ 1 ]")

        assert self.test.run("- add: [  2,  3 ] ") == 5
        assert self.test.run("- add: [ -2,  3 ] ") == 1
        assert self.test.run("- add: [  2, -3 ] ") == -1
        assert self.test.run("- add: [ -2, -3 ] ") == -5
        assert self.test.run("- add: [  1,  2, 3 ] ") == 6

    def test_sub(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- sub: []")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- sub: [ 1 ]")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- sub: [ 1, 2, 3 ]")

        assert self.test.run("- sub: [  2,  3 ] ") == -1
        assert self.test.run("- sub: [ -2,  3 ] ") == -5
        assert self.test.run("- sub: [  2, -3 ] ") == 5
        assert self.test.run("- sub: [ -2, -3 ] ") == 1

    def test_mul(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- mul: []")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- mul: [ 1 ]")

        assert self.test.run("- mul: [  2,  3 ] ") == 6
        assert self.test.run("- mul: [ -2,  3 ] ") == -6
        assert self.test.run("- mul: [  2, -3 ] ") == -6
        assert self.test.run("- mul: [ -2, -3 ] ") == 6
        assert self.test.run("- mul: [  2,  3, 4 ] ") == 24

    def test_div(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- div: []")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- div: [ 1 ]")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- div: [ 1, 2, 3 ]")

        assert '%.3f' % self.test.run("- div: [  2,  3 ] ") == '0.667'
        assert '%.3f' % self.test.run("- div: [ -2,  3 ] ") == '-0.667'
        assert '%.3f' % self.test.run("- div: [  2, -3 ] ") == '-0.667'
        assert '%.3f' % self.test.run("- div: [ -2, -3 ] ") == '0.667'

    def test_mod(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- mod: []")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- mod: [ 1 ]")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- mod: [ 1, 2, 3 ]")

        assert self.test.run("- mod: [  2,  3 ] ") == 2
        assert self.test.run("- mod: [ -2,  3 ] ") == 1
        assert self.test.run("- mod: [  2, -3 ] ") == -1
        assert self.test.run("- mod: [ -2, -3 ] ") == -2

    def test_exp(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- exp: []")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- exp: [ 1 ]")

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- exp: [ 1, 2, 3 ]")

        assert self.test.run("- exp: [  2,  3 ] ") == 8
        assert self.test.run("- exp: [ -2,  3 ] ") == -8
        assert self.test.run("- exp: [  2, -3 ] ") == 0.125
        assert self.test.run("- exp: [ -2, -3 ] ") == -0.125
