import sys
from mdsl.core import Mdsl


def mdsl_fail(context: Mdsl, args):
    '''
    Print data to standard error output and exit.
    Args:
    - msg: the message to print to stderr.
    Returns: nothing
    '''

    msg, error_code = context.arg_parser(args, [
        ('msg', ''),
        ('code', 1)
    ])
    print('' if msg is None else msg, file=sys.stderr)
    sys.exit(error_code)


class TestPrint:
    from mdsl.mdsl_test import MdslTest
    test = MdslTest({
        'fail': mdsl_fail
    })

    def test_fail_no_array(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- fail:")

    def test_fail_empty(self, capsys):
        import pytest
        with pytest.raises(SystemExit) as e:
            self.test.run("- fail: []")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == '\n'
        assert e.value.code == 1

    def test_fail_message(self, capsys):
        import pytest
        with pytest.raises(SystemExit) as e:
            self.test.run("- fail: [ 'oops' ]")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == 'oops\n'
        assert e.value.code == 1

    def test_fail_code(self, capsys):
        import pytest
        with pytest.raises(SystemExit) as e:
            self.test.run("- fail: { msg: 'oops', code: 42 }")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == 'oops\n'
        assert e.value.code == 42

    def test_fail_array_args(self, capsys):
        import pytest
        with pytest.raises(SystemExit) as e:
            self.test.run("- fail: [ 'oops', 42 ]")
        captured = capsys.readouterr()

        assert captured.out == ''
        assert captured.err == 'oops\n'
        assert e.value.code == 42
