import json
from typing import Dict, List, Tuple, Any, Callable, Union

class MdslError(SyntaxError): pass
class MdslUnknownFunction(MdslError): pass
class MdslArgumentError(MdslError): pass
class MdslNotDefined(MdslError): pass


class Mdsl:
    'Create a MDSL context on which mdsl code can be executed.'

    def __init__(self, functions: Dict[str, Callable]) -> None:
        self.functions = functions
        self.variables = {
            'PI': 3.1415
        }

    def arg_parser(self, data: Union[ list, Dict[str, Any] ], params: List[Tuple[str, Any]]) -> list:
        'Parse arguments of a mdsl function, that can be a list, a dict or a primitive type.'

        # args are not a list or dict
        if type(data) not in [ list, dict ]:
            raise MdslArgumentError('A list or a dict must be provided.')

        # variable args (only if list is provided)
        if len(params) == 0:
            if type(data) != list:
                raise MdslArgumentError('A list must be provided for variable-length parameters.')
            return list(data)

        params_key, params_default = list(zip(*params))
        nb_required = len(list(filter(lambda x: x is None, params_default)))

        # to many args
        if len(data) > len(params):
            raise MdslArgumentError(f'Too many arguments: { len(data) }' \
                + '\nExpected ones of: ' + ', '.join(params_key))

        # to many args
        if len(data) < nb_required:
            raise MdslArgumentError('Not enought arguments: ' + json.dumps(data) \
                + '\nExpected ones of: ' + ', '.join(params_key))

        # dict args
        if type(data) == dict:
            data = dict(data)

            # check unknown parameters
            for data_key in data.keys():
                if data_key not in params_key:
                    raise MdslArgumentError(f'unknown parameter: `{ data_key }`')

            # check mandatory parameters
            for param_key, param_default in params:
                if param_key not in data.keys() and param_default is None:
                    raise MdslArgumentError(f'mandatory parameter: `{ param_key }`')

            # set defaults
            data = [ data.get(key, default) for key, default in params ]

        data = list(data)

        if len(data) < nb_required:
            raise MdslArgumentError(f'mandatory parameters: `{ ", ".join(params_key) }`')

        if len(data) < len(params):
            data = [ data[id] if id < len(data) else default \
                for id, default in enumerate(params_default) ]

        # process data
        data = list(data)
        for i, arg in enumerate(data):
            if type(arg) == str and arg.startswith('$'):
                if arg[1:] in self.variables:
                    data[i] = self.variables[arg[1:]]
                else:
                    raise MdslNotDefined(f'Variable { arg } is not defined.')

            elif type(arg) == dict:
                data[i] = self.process_expression(arg)

        return list(data)

    def process_expression(self, expression: Dict[str, Any]):
        if len(expression) != 1:
            raise MdslArgumentError(f'On "{ expression }":\nStatements must be singleton dictionaries')

        function_name, args = list(expression.items())[0]

        if function_name.startswith('$'):
            self.variables[function_name[1:]] = args
            return True
        else:
            try:
                return self.functions[function_name](self, args)
            except KeyError:
                raise MdslUnknownFunction(f'\n\nOn "{ expression }":\nUnrecognized function: { function_name }.')

    def process_block(self, block: List[Dict[str, Any]]):
        result = False

        for expression in block:
            if type(expression) != dict:
                raise MdslArgumentError(f'On "{ expression }":\nStatements must be dictionaries')

            result = self.process_expression(expression)

        return result
