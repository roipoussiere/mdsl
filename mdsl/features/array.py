from mdsl.core import Mdsl
from mdsl.core import MdslArgumentError


def mdsl_concat(context: Mdsl, data):
    '''
    Concat data in a list or a string.
    Args: variable number of items.
    Returns: The array or string
    '''

    items = context.arg_parser(data, [])

    if len(items) == 0:
        return []

    try:
        return ''.join(items)
    except TypeError:
        list = []
        for item in items:
            list += [ item ]
        return list

def mdsl_join(context: Mdsl, data):
    '''
    Join strings with a separator between each item.
    Args:
    - string.
    Returns: The array or string
    '''

    items, sep = context.arg_parser(data, [('items', None), ('sep', '')])

    try:
        return sep.join(items)
    except TypeError:
        raise MdslArgumentError('Items must be strings.')


class TestPrint:
    from mdsl.mdsl_test import MdslTest
    test = MdslTest({
        'cat': mdsl_concat,
        'join': mdsl_join
    })

    def test_cat_no_array(self):
        import pytest
        from mdsl.core import MdslArgumentError

        with pytest.raises(MdslArgumentError) as e:
            self.test.run("- cat: 'hello'")

    def test_cat_empty(self):
        assert self.test.run("- cat: []") == []

    def test_cat_one_item(self):
        assert self.test.run("- cat: [ 123 ]") == [ 123 ]

    def test_cat_array(self):
        assert self.test.run("- cat: [ 1, 2, 3 ]"), [ 1, 2, 3 ]

    def test_cat_string(self):
        assert self.test.run("- cat: [ 'ab', 'cd' ]"), 'abcd'

    def test_join_empty(self):
        assert self.test.run("- join: [ [] , ', ']") == ''

    def test_join_single(self):
        assert self.test.run("- join: [ [ 'ab' ], ', ' ]") == 'ab'

    def test_join_string(self):
        assert self.test.run("- join: [ [ 'ab', 'cd' ], ', ']"), 'ab, cd'

    def test_join_string_dict(self):
        assert self.test.run("- join: { items: [ 'ab', 'cd' ], sep: ', ' }"), 'ab, cd'
