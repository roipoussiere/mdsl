import yaml
from mdsl import Mdsl


class MdslTest:
    def __init__(self, functions: dict) -> None:
        self.mdsl = Mdsl(functions)

    def run(self, mdsl_yaml: str) -> None:
        dsl_root = yaml.load(mdsl_yaml, Loader=yaml.FullLoader)
        return self.mdsl.process_block(dsl_root)
