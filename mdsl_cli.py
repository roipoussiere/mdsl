#!/usr/bin/env python

import yaml
from sys import argv, exit as sys_exit

from mdsl import Mdsl
from mdsl.features._print import mdsl_print
from mdsl.features._assert import mdsl_assert
from mdsl.features._fail import mdsl_fail
from mdsl.features.array import mdsl_concat, mdsl_join
from mdsl.features.comparators import mdsl_lt, mdsl_gt, mdsl_le, mdsl_ge, mdsl_eq, mdsl_ne
from mdsl.features.simple_maths import mdsl_add, mdsl_sub, mdsl_mul, mdsl_div, mdsl_mod, mdsl_exp


if len(argv) != 2:
    sys_exit(f'usage: { argv[0] } file.dsl.yml')

mdsl = Mdsl({
    'print': mdsl_print,
    'assert': mdsl_assert,
    'fail': mdsl_fail,
    'cat': mdsl_concat,
    'join': mdsl_join,
    'lt': mdsl_lt,
    'gt': mdsl_gt,
    'le': mdsl_le,
    'ge': mdsl_ge,
    'eq': mdsl_eq,
    'ne': mdsl_ne,
    'add': mdsl_add,
    'sub': mdsl_sub,
    'mul': mdsl_mul,
    'div': mdsl_div,
    'mod': mdsl_mod,
    'exp': mdsl_exp
})

with open(argv[1], 'r') as dsl_yaml:
    dsl_root = yaml.load(dsl_yaml, Loader=yaml.FullLoader)
    mdsl.process_block(dsl_root)
